all:
	ansible-playbook -i inventory.yaml playbook.yaml --ask-become-pass --ask-vault-pass

galaxy:
	ansible-galaxy install -r requirements.yaml

encrypt_ssh_keys:
	tar -czf ./files/ssh_keys.tar.gz --exclude 'known_hosts*' --exclude 'config' -C ~/.ssh .
	ansible-vault encrypt --vault-id ssh@prompt ./files/ssh_keys.tar.gz

decrypt_ssh_keys:
	ansible-vault decrypt --vault-id ssh@prompt ./files/ssh_keys.tar.gz
	mkdir -p ~/.ssh
	tar -czf ./files/ssh_keys.tar.gz --exclude 'known_hosts*' --exclude 'config' -C ~/.ssh .
	chmod 600 ~/.ssh/*
	ansible-vault encrypt --vault-id ssh@prompt ./files/ssh_keys.tar.gz

vpn:
	ansible-playbook -i inventory.yaml playbook.yaml --ask-become-pass --tags vpn

ssh:
	ansible-playbook -i inventory.yaml playbook.yaml --ask-become-pass --ask-vault-pass --tags ssh

dotfiles:
	ansible-playbook -i inventory.yaml playbook.yaml --ask-become-pass --ask-vault-pass --tags dotfiles

packages:
	ansible-playbook -i inventory.yaml playbook.yaml --ask-become-pass --tags packages,flatpak,rust,docker

tags := ""

tag:
	ansible-playbook -i inventory.yaml playbook.yaml --ask-become-pass --ask-vault-pass --tags "${tags}"
