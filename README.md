# ansible-testing
[![pipeline status](https://gitlab.com/jkeyne/ansible_setup/badges/main/pipeline.svg)](https://gitlab.com/jkeyne/ansible_setup/-/commits/main)
[![coverage report](https://gitlab.com/jkeyne/ansible_setup/badges/main/coverage.svg)](https://gitlab.com/jkeyne/ansible_setup/-/commits/main)
[![Latest Release](https://gitlab.com/jkeyne/ansible_setup/-/badges/release.svg)](https://gitlab.com/jkeyne/ansible_setup/-/releases)

* This is a repo for my ansible playbook to setup my machines running OpenSUSE Tumbleweed

## TODO
- [x] Automate PIA install
    - 'https://www.privateinternetaccess.com/download/linux-vpn' to parse for 'https://installers.privateinternetaccess.com/download/pia-linux-3.3.1-06924.run' to get latest file
        - `curl -s 'https://www.privateinternetaccess.com/download/linux-vpn' | grep -o 'https://.*pia-linux-[0-9\.\-]*.run' | sort -u | tail -n 1`
    - then run that .run file
    - TODO: still need to automate the user interaction though. right now there's just an interactive terminal popup i guess
- [ ] Look into [ansible-pull](https://docs.ansible.com/ansible/latest/cli/ansible-pull.html) to automatically pull the playbook and run it
    - seems interesting, would need a way to load the become and vault passwords automatically though for it to be actually useful
- [ ] Look into [this restic role](https://github.com/roles-ansible/ansible_role_restic) to setup restic backups
- [ ] Look into [this docker role](https://galaxy.ansible.com/geerlingguy/docker) instead of making my own role
- [x] Figure out a ~~good~~ way to test this automatically
    - ~~[ansible-builder](https://ansible.readthedocs.io/projects/builder/en/stable/) might be good~~
    - ~~`molecule` can at least test roles, could see if it's possible to test playbooks too~~
    - could also just use `docker`/`earthly` or whatever CI/CD pipeline to test it in a normal container
- [x] Look into automating network manager VPN configurations
    - this could be used to import a PIA OpenVPN file
    - this could also be used to setup my work VPN
- [ ] Automate tailscale install